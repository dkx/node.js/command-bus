import {Middleware, MiddlewareNextCallback} from './middleware';
import {Command} from './command';


declare type MiddlewareFunction<T> = (next: MiddlewareNextCallback<T>) => Promise<T>;


export class MiddlewareStack<T>
{


	constructor(
		private command: Command,
		private middlewares: Array<Middleware>,
		private runFinally: () => Promise<T>,
	) {}


	public async run(): Promise<T>
	{
		if (this.middlewares.length === 0) {
			return this.runFinally();
		}

		const boot: MiddlewareFunction<T> = (next: MiddlewareNextCallback<T>): Promise<T> => {
			return next();
		};

		return boot(this.createNextMiddlewareCallback(0));
	}


	private createNextMiddlewareCallback(index: number): MiddlewareNextCallback<T>
	{
		return (): Promise<T> => {
			if (typeof this.middlewares[index] === 'undefined') {
				return this.runFinally();
			}

			const middleware = this.middlewares[index];
			return middleware.apply(this.command, this.createNextMiddlewareCallback(index + 1));
		};
	}

}
