import {Command} from './command';
import {Handler} from './handler';
import {Middleware} from './middleware';
import {MiddlewareStack} from './middleware-stack';
import {Mapper} from './mapping';


export class CommandBus
{


	private middlewares: Array<Middleware> = [];


	constructor(
		private mapper: Mapper,
	) {}


	public use(middleware: Middleware): void
	{
		this.middlewares.push(middleware);
	}


	public async handle<T>(command: Command): Promise<T>
	{
		const handler = this.mapper.mapCommandToHandler<T>(command);
		return this.callHandler(command, handler);
	}


	private callHandler<T>(command: Command, handler: Handler<T>): Promise<T>
	{
		const stack = new MiddlewareStack(command, this.middlewares, () => {
			return Promise.resolve(handler.handle(command));
		});

		return stack.run();
	}

}
