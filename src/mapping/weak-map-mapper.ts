import {ClassType} from '@dkx/types-class';

import {Command} from '../command';
import {Handler} from '../handler';
import {Mapper} from './mapper';


export declare type HandlerFactory<T> = () => Handler<T>;

declare interface HandlerData<T>
{
	factory: HandlerFactory<T>,
	instance: Handler<T>|null,
}


export class WeakMapMapper implements Mapper
{


	private mapping: WeakMap<ClassType<Command>, HandlerData<any>> = new WeakMap;


	public add<T>(commandType: ClassType<Command>, handlerFactory: HandlerFactory<T>): void
	{
		this.mapping.set(commandType, {
			factory: handlerFactory,
			instance: null,
		});
	}


	public mapCommandToHandler<T>(command: Command): Handler<T>
	{
		const proto = Object.getPrototypeOf(command).constructor;

		if (!this.mapping.has(proto)) {
			throw new Error(`Handler for command ${proto.name} does not exists`);
		}

		const handlerData: HandlerData<T> = this.mapping.get(proto);

		if (handlerData.instance === null) {
			handlerData.instance = handlerData.factory();
		}

		return handlerData.instance
	}

}
