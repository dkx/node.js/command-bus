import {Command} from '../command';
import {Handler} from '../handler';

export interface Mapper
{


	mapCommandToHandler<T>(command: Command): Handler<T>;

}
