import {Command} from './command';


export declare type MiddlewareNextCallback<T> = () => Promise<T>;


export interface Middleware
{


	apply<T>(command: Command, next: MiddlewareNextCallback<T>): Promise<T>;

}
