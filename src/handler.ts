import {Command} from './command';


export interface Handler<T>
{


	handle(command: Command): T|Promise<T>;

}
