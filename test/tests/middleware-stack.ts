import {expect, use as chaiUse} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';

import {Command, Middleware, MiddlewareNextCallback} from '../../src';
import {MiddlewareStack} from '../../src/middleware-stack';

chaiUse(chaiAsPromised);


describe('#MiddlewareStack', () => {

	describe('run()', () => {

		it('should run stack without middlewares', async () => {
			class TestCommand implements Command {}

			const stack = new MiddlewareStack(new TestCommand, [], async () => {
				return true;
			});

			await expect(stack.run()).to.eventually.become(true);
		});

		it('should run stack with middlewares', async () => {
			let called: Array<string> = [];

			class TestCommand implements Command {}

			class MiddlewareWrap implements Middleware
			{
				public apply<T>(command: Command, next: MiddlewareNextCallback<T>): Promise<T>
				{
					expect(command).to.be.an.instanceOf(TestCommand);

					called.push('wrap.before');
					const result = next();
					called.push('wrap.after');
					return result;
				}
			}

			class MiddlewareSimple implements Middleware
			{
				public apply<T>(command: Command, next: MiddlewareNextCallback<T>): Promise<T>
				{
					expect(command).to.be.an.instanceOf(TestCommand);

					called.push('simple');
					return next();
				}
			}

			const stack = new MiddlewareStack(new TestCommand, [
				new MiddlewareWrap,
				new MiddlewareSimple,
			], async () => {
				return true;
			});

			await expect(stack.run()).to.eventually.become(true);

			expect(called).to.be.eql([
				'wrap.before',
				'simple',
				'wrap.after',
			]);
		});

	});

});
