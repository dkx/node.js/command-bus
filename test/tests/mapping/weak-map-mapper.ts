import {expect, use as chaiUse} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';

import {Command, Handler, WeakMapMapper} from '../../../src';

chaiUse(chaiAsPromised);


let mapper: WeakMapMapper;


describe('#Mapping/WeakMapMapper', () => {

	beforeEach(() => {
		mapper = new WeakMapMapper;
	});

	describe('mapCommandToHandler()', () => {

		it('should ensure that the handler factory is called only once', async () => {
			class TestCommand implements Command {}
			class TestHandler implements Handler<boolean>
			{
				public handle(command: TestCommand): boolean
				{
					return true;
				}
			}

			let called = 0;
			mapper.add(TestCommand, () => {
				called++;
				return new TestHandler;
			});

			const command = new TestCommand;

			expect(mapper.mapCommandToHandler(command)).to.be.an.instanceOf(TestHandler);
			expect(mapper.mapCommandToHandler(command)).to.be.an.instanceOf(TestHandler);
			expect(mapper.mapCommandToHandler(command)).to.be.an.instanceOf(TestHandler);

			expect(called).to.be.equal(1);
		});

	});

});
