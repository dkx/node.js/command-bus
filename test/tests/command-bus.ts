import {expect, use as chaiUse} from 'chai';
import * as chaiAsPromised from 'chai-as-promised';

import {Command, CommandBus, Handler, Middleware, MiddlewareNextCallback, WeakMapMapper} from '../../src';

chaiUse(chaiAsPromised);


let mapper: WeakMapMapper;
let bus: CommandBus;


describe('#CommandBus', () => {

	beforeEach(() => {
		mapper = new WeakMapMapper;
		bus = new CommandBus(mapper);
	});

	describe('handle()', () => {

		it('should throw an error if handler for command does not exists', async () => {
			class TestCommand implements Command {}
			const command = new TestCommand;
			await expect(bus.handle(command)).to.be.rejectedWith(Error, 'Handler for command TestCommand does not exists');
		});

		it('should handle command', async () => {
			class TestCommand implements Command {}
			class TestHandler implements Handler<boolean>
			{
				public handle(command: TestCommand): boolean
				{
					return true;
				}
			}

			mapper.add(TestCommand, () => new TestHandler);

			await expect(bus.handle(new TestCommand)).to.eventually.become(true);
		});

		it('should apply all middlewares', async () => {
			const called: Array<string> = [];

			class TestCommand implements Command {}
			class TestHandler implements Handler<boolean>
			{
				public handle(command: TestCommand): boolean
				{
					return true;
				}
			}

			class MiddlewareWrap implements Middleware
			{
				public apply<T>(command: Command, next: MiddlewareNextCallback<T>): Promise<T>
				{
					expect(command).to.be.an.instanceOf(TestCommand);

					called.push('wrap.before');
					const result = next();
					called.push('wrap.after');
					return result;
				}
			}

			class MiddlewareSimple implements Middleware
			{
				public apply<T>(command: Command, next: MiddlewareNextCallback<T>): Promise<T>
				{
					expect(command).to.be.an.instanceOf(TestCommand);

					called.push('simple');
					return next();
				}
			}

			mapper.add(TestCommand, () => new TestHandler);
			bus.use(new MiddlewareWrap);
			bus.use(new MiddlewareSimple);

			await bus.handle(new TestCommand);

			expect(called).to.be.eql([
				'wrap.before',
				'simple',
				'wrap.after',
			]);
		});

	});

});
